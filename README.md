# Codeberg Documentation
This repository contains the documentation for Codeberg, with some code to build it into
a static website.

Please have a look into it and consider to help writing the Documentation. This is still very much work-in-progress, the more useful material we collect, the better we can later present it! All contributions are very welcome!


## Usage
### Local Development
If you want to work on the documentation, for example by writing your own articles,
the easiest way to do so is to fork the documentation repository and develop locally.

First, run

```npm install```

to install all dependencies (they will be installed only for this project, not globally).
You only have to do this once.

Then run

```npm run serve```

to start a development web server that by default is listening at `http://localhost:8080`.

Now you can simply change, add or remove files, save them and the development server
should automatically reload all changed pages using the amazing Browsersync.

When you're done, commit your changes to your fork and write a pull request for
Codeberg/Documentation. We're happy about every contribution!

### Build & Deployment
Like for local development, before building and deploying you first have to install
the dependencies (once):

```npm install```

To build the entire website to the `_site` directory run

```npm run build```

Instead, to directly publish the page to Codeberg pages, you can also run

```npm run deploy```

which includes a call to `npm run build`.

### Technical Information
This website uses [Eleventy](https://www.11ty.dev/), a static site generator.

It's supplied as a dev-dependency in `package.json` and its dependencies are locked
with `package-lock.json` to try to ensure reproducible builds.


## License and Contributors
This website (excluding bundled fonts) is licensed under CC BY-SA 4.0. See the [LICENSE](LICENSE.md) file for details.

The contributors are listed in `CONTRIBUTORS.md`.