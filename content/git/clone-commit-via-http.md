---
eleventyNavigation:
  key: CloneCommitViaHTTP
  title: Clone & Commit via HTTP
  parent: Git
  order: 20
---

Clone, edit, commit, push and pull can be performed using Git directly from the command line, by using a Git client, or via the web interface. The first option is shown below and in the section [Clone & Commit via SSH](/content/git/clone-commit-via-ssh). The last option is detailed in the section [Clone & Commit via Web](/content/git/clone-commit-via-web).

The user in this examples is `knut` the polar bear and it's repository is `foobar`. The repository was created via the Codeberg website including a `README.md` file.

## Clone
*Cloning* refers to the process of creating an identical copy of a repository to the local machine.  
Clone with the Git command `clone` followed by the repo URL.

```bash
~$ git clone https://codeberg.org/knut/foobar.git
Cloning into 'foobar'...
remote: Enumerating objects: 3, done.
remote: Counting objects: 100% (3/3), done.
remote: Total 3 (delta 0), reused 0 (delta 0)
Unpacking objects: 100% (3/3), 214 bytes | 1024 bytes/s, done.
```

## Edit
Modify an existing file:

```bash
~$ cd foobar
~/foobar$ vim README.md
```

## Commit
A *commit* is a record of the changes to the repository. This is like a snapshot of your edits.  
A commit requires a commit message. For the example below, the message is "test". Keep in mind that "test" is not a very informative message, though. In the real world, make sure your commit message is informative, for you, your collaborators and anyone who might be interested in your work. Some advice on how to write a good commit message can be found on countless websites and blogs!

Command lines:

```bash
~/foobar$ git commit -am 'test'
[master 10074d7] test
 1 file changed, 2 insertions(+), 1 deletion(-)
```

## Push
The last step is to synchronize (*push*) the modifications (commit) from the local repository to the remote one on Codeberg.  

```bash
~/foobar$ git push https://codeberg.org/knut/foobar.git
Username for 'https://codeberg.org': knut
Password for 'https://knut@codeberg.org': 
Counting objects: 3, done.
Delta compression using up to 4 threads.
Compressing objects: 100% (2/2), done.
Writing objects: 100% (3/3), 266 bytes | 0 bytes/s, done.
Total 3 (delta 1), reused 0 (delta 0)
To https://codeberg.org/knut/foobar.git
   662e04e..10074d7  master -> master
```

## Pull
*Pulling* synchronizes the modifications (commit) from the remote repository on Codeberg to the local one.  
Pulling is important when you work on different computers to make sure that all computers are on the same stage. It is even more important when you have collaborators on a project; they might change the files too, so you need to pull these modifications before you start working.  
Because of that, it is recommended to pull before pushing.
